﻿namespace Tripadvisor.Application.Contract.Locations.Queries
{
    public class LocationQueryModel
    {
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string Description { get; set; }
        public PopularityRankingModel Ranking { get; set; }
        public int Benchmark { get; set; }
        public bool Highlighted { get; set; }

        public class PopularityRankingModel
        {
            public int GeolocationId { get; set; }
            public int Rank { get; set; }
            public int RankOutOf { get; set; }
        }
    }
}
