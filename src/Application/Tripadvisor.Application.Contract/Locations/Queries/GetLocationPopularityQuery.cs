﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Text.Json.Serialization;
using Tripadvisor.Application.Contract.Framework;

namespace Tripadvisor.Application.Contract.Locations.Queries
{
    public class GetLocationPopularityQuery : IQuery<LocationQueryModel>
    {
        [JsonIgnore]
        [BindNever]
        public int LocationId { get; set; }
    }
}
