﻿using Tripadvisor.Application.Contract.Framework;

namespace Tripadvisor.Application.Contract.Locations.Queries
{
    public class LocationBenchMarkQueryModel: QueryModel<int>
    {
        public int LocationId { get; set; }
        public byte Rank { get; set; }
    }
}
