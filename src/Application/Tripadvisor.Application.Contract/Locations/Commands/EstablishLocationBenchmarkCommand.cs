﻿using Tripadvisor.Application.Contract.Framework;

namespace Tripadvisor.Application.Contract.Locations.Commands
{
    public class EstablishLocationBenchmarkCommand: ICommand
    {
        public int LocationId { get; set; }
        public byte Rank { get; set; }
    }
}
