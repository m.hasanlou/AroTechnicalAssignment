﻿using System;

namespace Tripadvisor.Application.Contract.Framework;

public class QueryModel<T>
{
    public T Id { get; set; }
    public DateTime CreationDate { get; set; }

}