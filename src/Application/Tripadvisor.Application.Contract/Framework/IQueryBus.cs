﻿using System.Threading.Tasks;

namespace Tripadvisor.Application.Contract.Framework;

public interface IQueryBus
{
    Task<TResult> Dispatch<TQuery, TResult>(TQuery query)
        where TQuery : IQuery<TResult>;
}