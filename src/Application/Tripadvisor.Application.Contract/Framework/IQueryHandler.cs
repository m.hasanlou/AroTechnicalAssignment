﻿using System.Threading;
using System.Threading.Tasks;

namespace Tripadvisor.Application.Contract.Framework;

public interface IQueryHandler<in TQuery, TResult> where TQuery : IQuery<TResult>
{
    Task<TResult> HandleAsync(
        TQuery query, CancellationToken cancellation = default);
}