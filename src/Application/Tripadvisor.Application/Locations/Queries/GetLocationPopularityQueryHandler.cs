﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tripadvisor.Application.Contract.Framework;
using Tripadvisor.Application.Contract.Locations.Queries;
using Tripadvisor.Domain.Locations;

namespace Tripadvisor.Application.Locations.Queries
{
    public class GetLocationPopularityQueryHandler
        : IQueryHandler<GetLocationPopularityQuery, LocationQueryModel>
    {
        private readonly ILocationPopularityRetriever _popularityRetriever;

        public GetLocationPopularityQueryHandler(
            ILocationPopularityRetriever popularityRetriever)
        {
            _popularityRetriever = popularityRetriever;
        }

        public async Task<LocationQueryModel> HandleAsync(
            GetLocationPopularityQuery query, CancellationToken cancellation = default)
        {
            var popularity = await _popularityRetriever.Retrieve(query.LocationId, cancellation);

            if (popularity is null) return null;

            return new LocationQueryModel
            {
                LocationId = popularity.LocationId,
                LocationName = popularity.LocationName,
                Description = popularity.Description,
                Benchmark = popularity.Benchmark,
                Highlighted = popularity.Highlighted,
                Ranking = new LocationQueryModel.PopularityRankingModel
                {
                    GeolocationId = popularity.Ranking.GeolocationId,
                    Rank = popularity.Ranking.Rank,
                    RankOutOf = popularity.Ranking.RankOutOf
                }
            };
        }
    }
}
