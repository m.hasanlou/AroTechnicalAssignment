﻿using Tripadvisor.Application.Contract.Framework;
using Tripadvisor.Application.Contract.Locations.Commands;
using Tripadvisor.Application.Contract.Locations.Queries;
using Tripadvisor.Domain.Framework;
using Tripadvisor.Domain.Locations;

namespace Tripadvisor.Application.Locations.Commands
{
    public class EstablishLocationBenchmarkCommandHandler
        : ICommandHandler<EstablishLocationBenchmarkCommand, LocationBenchMarkQueryModel>
    {
        private readonly IUnitOfWork _uow;
        private readonly ICacheManager _cacheManager;

        public EstablishLocationBenchmarkCommandHandler(
            IUnitOfWork uow,
            ICacheManager cacheManager)
        {
            _uow = uow;
            _cacheManager = cacheManager;
        }

        public async Task<LocationBenchMarkQueryModel> HandleAsync(
            EstablishLocationBenchmarkCommand command, CancellationToken cancellation = default)
        {
            var model = new LocationBenchMark(command.LocationId, command.Rank);

            var entity = await _uow.LocationBenchmarkRepository.Add(model, cancellation);
            await _uow.SaveChanges(cancellation);

            _cacheManager.Set($"{nameof(LocationBenchMark)}_{entity.GeoLocationId}", entity);

            return new LocationBenchMarkQueryModel
            {
                Id = entity.Id,
                CreationDate = entity.CreationDate,
                LocationId = entity.GeoLocationId,
                Rank = entity.Rank
            };
        }
    }
}
