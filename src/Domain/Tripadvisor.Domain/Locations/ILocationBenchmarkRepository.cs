﻿namespace Tripadvisor.Domain.Locations;

public interface ILocationBenchmarkRepository
{
    Task<LocationBenchMark?> GetByLocation(
        int locationId, CancellationToken cancellation = default);
    Task<LocationBenchMark> Add(
        LocationBenchMark model, CancellationToken cancellation = default);
}