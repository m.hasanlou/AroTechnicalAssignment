﻿namespace Tripadvisor.Domain.Locations
{
    public interface ILocationPopularityRetriever
    {
        Task<LocationPopularity?> Retrieve(int locationId, CancellationToken cancellation = default);
    }

    public class LocationPopularity
    {
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string Description { get; set; }
        public PopularityRanking Ranking { get; set; }
        public int Benchmark { get; set; }
        public bool Highlighted => Benchmark != Ranking.Rank;

        public class PopularityRanking
        {
            public int GeolocationId { get; set; }
            public int Rank { get; set; }
            public int RankOutOf { get; set; }
        }
    }

}
