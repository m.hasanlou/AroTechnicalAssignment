﻿using Tripadvisor.Domain.Framework;

namespace Tripadvisor.Domain.Locations
{
    public class LocationBenchMark : Entity<int>
    {
        public LocationBenchMark(int geoLocationId, byte rank)
        {
            Guard.NotNullOrDefault(geoLocationId, nameof(GeoLocationId));
            Guard.NotNullOrDefault(rank, nameof(Rank));

            GeoLocationId = geoLocationId;
            Rank = rank;
        }

        public int GeoLocationId { get; private set; }
        public byte Rank { get; private set; }

        public void ModifyRank(byte rank)
        {
            Guard.NotNullOrDefault(rank, nameof(Rank));

            Rank = rank;
        }
    }
}
