﻿namespace Tripadvisor.Domain.Framework;

public abstract class Entity<T>
{
    protected Entity()
    {
        CreationDate = DateTime.UtcNow;
    }

    public T Id { get; protected set; }
    public bool Removed { get; private set; }
    public DateTime CreationDate { get; private set; }

    protected void Remove()
    {
        Removed = true;
    }
}