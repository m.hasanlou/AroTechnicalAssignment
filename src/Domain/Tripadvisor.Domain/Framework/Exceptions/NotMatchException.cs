﻿using Framework.Core.Resources;

namespace Tripadvisor.Domain.Framework.Exceptions
{
    public class NotMatchException : BusinessException
    {
        public NotMatchException(string firstParam, string secondParam)
            : base(BusinessExceptionCode.InvalidData,
                string.Format(ExceptionMessages.NotMatch, firstParam, secondParam))
        {
        }
    }
}