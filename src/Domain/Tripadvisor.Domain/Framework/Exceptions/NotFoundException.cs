﻿using Framework.Core.Resources;

namespace Tripadvisor.Domain.Framework.Exceptions
{
    public class NotFoundException: BusinessException
    {
        public NotFoundException(string entityName) 
            : base(BusinessExceptionCode.NotFound,
                string.Format(ExceptionMessages.NotFound, entityName))
        {
        }
    }
}
