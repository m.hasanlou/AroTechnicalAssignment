﻿using Framework.Core.Resources;

namespace Tripadvisor.Domain.Framework.Exceptions
{
    public class MinLengthException: BusinessException
    {
        public MinLengthException(string paramName, int minLength) 
            : base(BusinessExceptionCode.InvalidData,
                string.Format(ExceptionMessages.MinLength, paramName, minLength))
        {
        }
    }
}