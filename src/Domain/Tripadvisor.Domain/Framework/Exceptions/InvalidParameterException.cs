﻿using Framework.Core.Resources;

namespace Tripadvisor.Domain.Framework.Exceptions
{
    public class InvalidParameterException : BusinessException
    {
        public InvalidParameterException(string paramName)
            : base(BusinessExceptionCode.InvalidData,
                string.Format(ExceptionMessages.InvalidData, paramName))
        {
        }
    }
}
