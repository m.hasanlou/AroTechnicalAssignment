﻿using Framework.Core.Resources;

namespace Tripadvisor.Domain.Framework.Exceptions
{
    public class AlreadyExistsException : BusinessException
    {
        public AlreadyExistsException(string name)
            : base(BusinessExceptionCode.Duplicate,
                string.Format(ExceptionMessages.AlreadyExist, name))
        {
        }
    }
}