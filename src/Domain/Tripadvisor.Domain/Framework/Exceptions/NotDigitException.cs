﻿using Framework.Core.Resources;

namespace Tripadvisor.Domain.Framework.Exceptions
{
    public class NotDigitException: BusinessException
    {
        public NotDigitException(string paramName) 
            : base(BusinessExceptionCode.InvalidData,
                string.Format(ExceptionMessages.NotDigit, paramName))
        {
        }
    }
}