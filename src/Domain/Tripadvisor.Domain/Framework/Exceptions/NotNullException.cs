﻿using Framework.Core.Resources;

namespace Tripadvisor.Domain.Framework.Exceptions
{
    public class NotNullException: BusinessException
    {
        public NotNullException(string paramName) 
            : base(BusinessExceptionCode.NotNul,
                string.Format(ExceptionMessages.NotNull, paramName))
        {
        }
    }
}