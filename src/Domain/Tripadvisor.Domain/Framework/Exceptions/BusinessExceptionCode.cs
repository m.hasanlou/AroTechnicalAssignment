﻿namespace Tripadvisor.Domain.Framework.Exceptions;

public enum BusinessExceptionCode
{
    UnHandledException = 100,
    OperationFail = 101,
    NotFound = 102,
    InvalidData = 103,
    OutOfRange = 104,
    NotEmpty = 105,
    NotNul = 106,
    InvalidStatus = 107,
    Duplicate = 108
}