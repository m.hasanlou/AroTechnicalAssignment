using Framework.Core.Resources;

namespace Tripadvisor.Domain.Framework.Exceptions
{
    public class OutOfRangeException : BusinessException
    {
        public OutOfRangeException(string name)
            : base(BusinessExceptionCode.OutOfRange,
                string.Format(ExceptionMessages.OutOfRange, name))
        {
        }
    }
}