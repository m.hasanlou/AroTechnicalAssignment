﻿using Framework.Core.Resources;

namespace Tripadvisor.Domain.Framework.Exceptions
{
    public class OperationFailedException: BusinessException
    {
        public OperationFailedException(string operation) 
            : base(BusinessExceptionCode.OperationFail,
                string.Format(ExceptionMessages.OperationFailed, operation))
        {
        }
    }
}