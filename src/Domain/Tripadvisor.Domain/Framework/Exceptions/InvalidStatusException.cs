﻿using Framework.Core.Resources;

namespace Tripadvisor.Domain.Framework.Exceptions
{
    public class InvalidStatusException : BusinessException
    {
        public InvalidStatusException(string entityName)
            : base(BusinessExceptionCode.InvalidStatus,
                string.Format(ExceptionMessages.InvalidStatus, entityName))
        {
        }
    }
}