﻿using Framework.Core.Resources;

namespace Tripadvisor.Domain.Framework.Exceptions
{
    public class NotEmptyException: BusinessException
    {
        public NotEmptyException(string paramName) 
            : base(BusinessExceptionCode.NotEmpty,
                string.Format(ExceptionMessages.NotEmpty, paramName))
        {
        }
    }
}