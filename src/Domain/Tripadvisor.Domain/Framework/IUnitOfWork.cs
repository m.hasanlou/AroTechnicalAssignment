﻿using Tripadvisor.Domain.Locations;

namespace Tripadvisor.Domain.Framework
{
    public interface IUnitOfWork
    {
        public ILocationBenchmarkRepository LocationBenchmarkRepository { get; }
        Task<int> SaveChanges(CancellationToken cancellation = default);
    }
}
