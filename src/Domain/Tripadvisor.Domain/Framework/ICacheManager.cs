﻿namespace Tripadvisor.Domain.Framework
{
    public interface ICacheManager
    {
        void Set(string key, object value);
        bool Contains(string key);
        object? Get(string key);
        T? Get<T>(string key);
        void Remove(string key);
        void UpdateAsync(string key, object value);
        T GetOrAddAsync<T>(string key, Func<T> action);
    }
}
