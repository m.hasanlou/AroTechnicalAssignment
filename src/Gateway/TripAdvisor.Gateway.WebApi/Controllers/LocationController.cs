using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tripadvisor.Application.Contract.Framework;
using Tripadvisor.Application.Contract.Locations.Queries;

namespace Tripadvisor.Gateway.RestApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LocationController : ControllerBase
    {
        private readonly IQueryBus _queryBus;

        public LocationController(IQueryBus queryBus)
        {
            _queryBus = queryBus;
        }

        [HttpGet("{locationId}/[action]")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(LocationBenchMarkQueryModel))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces("application/json")]
        public async Task<ActionResult<LocationBenchMarkQueryModel>> Popularity(int locationId)
        {
            var query = new GetLocationPopularityQuery { LocationId = locationId };

            var popularity = await _queryBus.Dispatch<GetLocationPopularityQuery, LocationQueryModel>(query);

            if (popularity is null)
            {
                return NotFound();
            }

            return Ok(popularity);
        }
    }
}