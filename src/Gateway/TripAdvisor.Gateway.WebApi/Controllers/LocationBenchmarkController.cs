using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tripadvisor.Application.Contract.Framework;
using Tripadvisor.Application.Contract.Locations.Commands;
using Tripadvisor.Application.Contract.Locations.Queries;

namespace Tripadvisor.Gateway.RestApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LocationBenchmarkController : ControllerBase
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryBus _queryBus;

        public LocationBenchmarkController(
            ICommandBus commandBus, IQueryBus queryBus)
        {
            _commandBus = commandBus;
            _queryBus = queryBus;
        }

        [HttpGet("{locationId}/[action]")]
        [ProducesResponseType(StatusCodes.Status200OK,
            Type = typeof(LocationBenchMarkQueryModel))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Produces("application/json")]
        public ActionResult<LocationBenchMarkQueryModel> Get(int locationId)
        {
            //var locationBenchMark = ;

            //if (locationBenchMark is null)
            //{
            //    return NotFound();
            //}

            //return Ok(locationBenchMark);

            return Ok(5);
        }


        [HttpPost("[action]")]
        [ProducesResponseType(StatusCodes.Status201Created,
            Type = typeof(LocationBenchMarkQueryModel))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [Produces("application/json")]
        public async Task<ActionResult<LocationBenchMarkQueryModel>> EstablishBenchmark(
            [FromBody] EstablishLocationBenchmarkCommand command)
        {
            var locationBenchMark = await _commandBus.Dispatch<EstablishLocationBenchmarkCommand, LocationBenchMarkQueryModel>(command);
            if (locationBenchMark is not null)
            {
                return CreatedAtRoute(nameof(Get), new { id = locationBenchMark.Id }, locationBenchMark);
            }

            ModelState.AddModelError("", "Somethings went wrong.");
            return StatusCode(StatusCodes.Status500InternalServerError, ModelState);
        }
    }
}