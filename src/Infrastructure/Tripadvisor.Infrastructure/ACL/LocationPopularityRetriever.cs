﻿using Tripadvisor.Domain.Locations;
using Tripadvisor.Infrastructure.HttpUtility;

namespace Tripadvisor.Infrastructure.ACL
{
    public class LocationPopularityRetriever : ILocationPopularityRetriever
    {
        private readonly HttpClient _httpClient;
        private readonly ApiCaller _apiCaller;
        private readonly ILocationBenchmarkRepository _benchmarkRepository;

        public LocationPopularityRetriever(
            HttpClient httpClient,
            ApiCaller apiCaller,
            ILocationBenchmarkRepository benchmarkRepository)
        {
            _httpClient = httpClient;
            _apiCaller = apiCaller;
            _benchmarkRepository = benchmarkRepository;
        }

        public async Task<LocationPopularity?> Retrieve(
            int locationId, CancellationToken cancellation = default)
        {
            //var result = await _apiCaller.GetAsync<LocationDetail>(
            //    _httpClient, $"{locationId}/details");

            var locationDetail = new LocationDetail
            {
                location_id = locationId,
                name = "Hotel",
                description = "test",
                ranking_data = new LocationDetail.RankingData
                {
                    geo_location_id = 1,
                    ranking = new Random().Next(1, 9),
                    ranking_out_of = 10
                }
            };
            var locationBenchmark = await _benchmarkRepository.GetByLocation(
                locationDetail.ranking_data.geo_location_id, cancellation);

            return new LocationPopularity
            {
                LocationId = locationDetail.location_id,
                LocationName = locationDetail.name,
                Description = locationDetail.description,
                Benchmark = locationBenchmark?.Rank ?? 0,
                Ranking = new LocationPopularity.PopularityRanking
                {
                    GeolocationId = locationDetail.ranking_data.geo_location_id,
                    Rank = locationDetail.ranking_data.ranking,
                    RankOutOf = locationDetail.ranking_data.ranking_out_of
                }
            };
        }
    }
}
