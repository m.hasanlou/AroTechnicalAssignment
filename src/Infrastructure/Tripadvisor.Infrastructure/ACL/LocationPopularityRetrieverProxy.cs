﻿using Tripadvisor.Domain.Framework;
using Tripadvisor.Domain.Locations;

namespace Tripadvisor.Infrastructure.ACL;

public class LocationPopularityRetrieverProxy: ILocationPopularityRetriever
{
    private readonly ILocationPopularityRetriever _popularityRetriever;
    private readonly ICacheManager _cacheManager;

    public LocationPopularityRetrieverProxy(
        ILocationPopularityRetriever popularityRetriever,
        ICacheManager cacheManager)
    {
        _popularityRetriever = popularityRetriever;
        _cacheManager = cacheManager;
    }

    public async Task<LocationPopularity?> Retrieve(
        int locationId, CancellationToken cancellation = default)
    {
        var key = $"{nameof(LocationPopularity)}_{locationId}";
        var locationPopularity = _cacheManager.Get<LocationPopularity>(key);

        if (locationPopularity is not null)
        {
            return locationPopularity;
        }

        locationPopularity = await _popularityRetriever.Retrieve(locationId, cancellation);
        if (locationPopularity is not null)
        {
            //TODO: cache for 24 hours
            _cacheManager.Set(key, locationPopularity);
        }

        return locationPopularity;
    }
}