﻿namespace Tripadvisor.Infrastructure.ACL
{
    public class LocationDetail
    {
        public int location_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string web_url { get; set; }
        public AddressObj address_obj { get; set; }
        public List<Ancestor> ancestors { get; set; }
        public int latitude { get; set; }
        public int longitude { get; set; }
        public string timezone { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string website { get; set; }
        public string write_review { get; set; }
        public RankingData ranking_data { get; set; }
        public int rating { get; set; }
        public string rating_image_url { get; set; }
        public string num_reviews { get; set; }
        public ReviewRatingCount review_rating_count { get; set; }
        public Subratings subratings { get; set; }
        public int photo_count { get; set; }
        public string see_all_photos { get; set; }
        public string price_level { get; set; }
        public Hours hours { get; set; }
        public List<string> amenities { get; set; }
        public List<string> features { get; set; }
        public List<Cuisine> cuisine { get; set; }
        public string parent_brand { get; set; }
        public string brand { get; set; }
        public Category category { get; set; }
        public List<Subcategory> subcategory { get; set; }
        public List<Group> groups { get; set; }
        public List<string> styles { get; set; }
        public List<NeighborhoodInfo> neighborhood_info { get; set; }
        public List<TripType> trip_types { get; set; }
        public List<Award> awards { get; set; }
        public Error error { get; set; }


        public class Category
        {
            public string name { get; set; }
            public string localized_name { get; set; }
        }

        public class Category2
        {
            public string name { get; set; }
            public string localized_name { get; set; }
        }

        public class Close
        {
            public int day { get; set; }
            public string time { get; set; }
        }

        public class Cuisine
        {
            public string name { get; set; }
            public string localized_name { get; set; }
        }

        public class Error
        {
            public string message { get; set; }
            public string type { get; set; }
            public int code { get; set; }
        }

        public class Group
        {
            public string name { get; set; }
            public string localized_name { get; set; }
            public List<Category> categories { get; set; }
        }

        public class Hours
        {
            public List<Period> periods { get; set; }
            public List<string> weekday_text { get; set; }
            public List<Subcategory> subcategory { get; set; }
        }

        public class Images
        {
            public string tiny { get; set; }
            public string small { get; set; }
            public string large { get; set; }
        }

        public class NeighborhoodInfo
        {
            public string location_id { get; set; }
            public string name { get; set; }
        }

        public class Open
        {
            public int day { get; set; }
            public string time { get; set; }
        }

        public class Period
        {
            public Open open { get; set; }
            public Close close { get; set; }
        }

        public class RankingData
        {
            public int geo_location_id { get; set; }
            public string ranking_string { get; set; }
            public string geo_location_name { get; set; }
            public int ranking_out_of { get; set; }
            public int ranking { get; set; }
        }

        public class ReviewRatingCount
        {
            public string additionalProp { get; set; }
        }
        public class Subcategory
        {
            public string name { get; set; }
            public string localized_name { get; set; }
        }

        public class Subratings
        {
            public AdditionalProp additionalProp { get; set; }
        }

        public class TripType
        {
            public string name { get; set; }
            public string localized_name { get; set; }
            public string value { get; set; }
        }
        public class AdditionalProp
        {
            public string name { get; set; }
            public string localized_name { get; set; }
            public string rating_image_url { get; set; }
            public int value { get; set; }
        }
        public class AddressObj
        {
            public string street1 { get; set; }
            public string street2 { get; set; }
            public string city { get; set; }
            public string state { get; set; }
            public string country { get; set; }
            public string postalcode { get; set; }
            public string address_string { get; set; }
        }
        public class Ancestor
        {
            public string abbrv { get; set; }
            public string level { get; set; }
            public string name { get; set; }
            public int location_id { get; set; }
        }
        public class Award
        {
            public string award_type { get; set; }
            public int year { get; set; }
            public Images images { get; set; }
            public List<string> categories { get; set; }
            public string display_name { get; set; }
        }

    }

}