﻿using Microsoft.Extensions.Caching.Memory;
using Tripadvisor.Domain.Framework;

namespace Tripadvisor.Infrastructure;

public class MemoryCacheManager : ICacheManager
{
    private readonly MemoryCache _memoryCache;

    public MemoryCacheManager()
    {
        _memoryCache = new MemoryCache(
            new MemoryCacheOptions
            {
                SizeLimit = 2000,
                //CompactionPercentage = 0.1
            });
    }

    public void Set(string key, object value)
    {
        var cacheEntryOptions = new MemoryCacheEntryOptions()
            .SetSize(1);

        _memoryCache.Set(key, value, cacheEntryOptions);
    }

    public bool Contains(string key)
    {
        return _memoryCache.TryGetValue(key, out _);
    }

    public object? Get(string key)
    {
        return _memoryCache.TryGetValue(key, out var cachedItem) ? cachedItem : null;
    }

    public T? Get<T>(string key)
    {
        return _memoryCache.TryGetValue(key, out T cachedItem) ? cachedItem : default;
    }

    public void Remove(string key)
    {
        //TODO: Check if exist
        _memoryCache.Remove(key);
    }

    public void UpdateAsync(string key, object value)
    {
        Remove(key);
        Set(key, value);
    }

    public T GetOrAddAsync<T>(string key, Func<T> action)
    {
        if (_memoryCache.TryGetValue(key, out T cachedItem))
        {
            return cachedItem;
        }

        var value = action();
        if (value != null)
        {
            Set(key, value);
        }

        return value;
    }
}