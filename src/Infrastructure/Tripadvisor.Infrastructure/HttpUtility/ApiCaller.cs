﻿using Newtonsoft.Json;

namespace Tripadvisor.Infrastructure.HttpUtility;

public sealed class ApiCaller
{
    public async Task<HttpResponse<TResult>> GetAsync<TResult>(
        HttpClient httpClient, string requestUri)
    {
        var httpRequest = HttpRequestBuilder.New()
            .WithUri(requestUri)
            .WithGetVerb()
            .Build();

        return await SendAsync<TResult>(httpClient, httpRequest);
    }

    public async Task<HttpResponse<TResult>> PostAsync<TResult>(
        HttpClient httpClient, string requestUri, object data)
    {
        var httpRequest = HttpRequestBuilder.New()
            .WithUri(requestUri)
            .WithPostVerb()
            .AddJsonContent(data)
            .Build();

        return await SendAsync<TResult>(httpClient, httpRequest);
    }

    public async Task<HttpResponse<TResult>> PostFormAsync<TResult>(
        HttpClient httpClient, string requestUri, object data)
    {
        var httpRequest = HttpRequestBuilder.New()
            .WithUri(requestUri)
            .WithPostVerb()
            .AddFormContent(data)
            .Build();

        return await SendAsync<TResult>(httpClient, httpRequest);
    }

    public async Task<HttpResponse<TResult>> PutAsync<TResult>(
        HttpClient httpClient, string requestUri, object data)
    {
        var httpRequest = HttpRequestBuilder.New()
            .WithUri(requestUri)
            .WithPutVerb()
            .AddJsonContent(data)
            .Build();

        return await SendAsync<TResult>(httpClient, httpRequest);
    }

    public async Task<HttpResponse<TResult>> DeleteAsync<TResult>(
        HttpClient httpClient, string requestUri)
    {
        var httpRequest = HttpRequestBuilder.New()
            .WithUri(requestUri)
            .WithDeleteVerb()
            .Build();

        return await SendAsync<TResult>(httpClient, httpRequest);
    }

    public async Task<HttpResponse<TResult>> PatchAsync<TResult>(
        HttpClient httpClient, string requestUri, object data)
    {
        var httpRequest = HttpRequestBuilder.New()
            .WithUri(requestUri)
            .WithPatchVerb()
            .AddJsonContent(data)
            .Build();

        return await SendAsync<TResult>(httpClient, httpRequest);
    }

    public async Task<HttpResponse<TResult>> SendAsync<TResult>(
        HttpClient httpClient, HttpRequestMessage request)
    {
        var response = await httpClient.SendAsync(request);
        return await CreateResult<TResult>(response,
            request.Content != null ? await request.Content.ReadAsStringAsync() : string.Empty);
    }

    private async Task<HttpResponse<TResult>> CreateResult<TResult>(
        HttpResponseMessage response, string requestContent)
    {
        var resultContent = await response.Content.ReadAsStringAsync();
        var result = response.IsSuccessStatusCode && !string.IsNullOrEmpty(resultContent) ?
            JsonConvert.DeserializeObject<TResult>(
                resultContent, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })
            : default;

        return new HttpResponse<TResult>
        {
            RequestContent = requestContent,
            IsSuccess = response.IsSuccessStatusCode,
            Result = result,
            StatusCode = response.StatusCode,
            Message = !response.IsSuccessStatusCode ? resultContent : string.Empty
        };
    }
}