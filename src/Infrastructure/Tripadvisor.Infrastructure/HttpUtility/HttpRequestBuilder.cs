﻿using System.Text;
using Newtonsoft.Json;

namespace Tripadvisor.Infrastructure.HttpUtility;

public sealed class HttpRequestBuilder : IHttpRequestWithContentBuilder
{
    private string _uri;
    private HttpMethod _method;
    private Dictionary<string, string> _headers;
    private HttpContent _content;
    private HttpRequestBuilder()
    {
        _headers = new Dictionary<string, string>();
    }

    public static IHttpRequestBuilder New()
    {
        return new HttpRequestBuilder();
    }

    public IHttpRequestBuilder WithUri(string uri)
    {
        _uri = uri;
        return this;
    }
    public IHttpRequestBuilder WithGetVerb()
    {
        _method = HttpMethod.Get;
        return this;
    }
    public IHttpRequestWithContentBuilder WithPostVerb()
    {
        _method = HttpMethod.Post;
        return this;
    }
    public IHttpRequestWithContentBuilder WithPutVerb()
    {
        _method = HttpMethod.Put;
        return this;
    }
    public IHttpRequestWithContentBuilder WithPatchVerb()
    {
        _method = HttpMethod.Patch;
        return this;
    }
    public IHttpRequestBuilder WithDeleteVerb()
    {
        _method = HttpMethod.Delete;
        return this;
    }
    public IHttpRequestBuilder WithAccessToken(string token)
    {
        _headers.Add("Authorization", $"Bearer {token}");
        return this;
    }
    public IHttpRequestWithContentBuilder AddJsonContent(object data)
    {
        if (data is not null)
        {
            _content = new StringContent(
                JsonConvert.SerializeObject(data, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }),
                Encoding.UTF8, "application/json");
        }
        return this;
    }
    public IHttpRequestWithContentBuilder AddXmlContent(object data)
    {
        var stringData = JsonConvert.SerializeObject(data);
        _content = new StringContent(stringData, Encoding.UTF8, "text/xml");
        return this;
    }
    public IHttpRequestWithContentBuilder AddFormContent(object data)
    {
        var stringData = JsonConvert.SerializeObject(data);
        _content = new StringContent(stringData, Encoding.UTF8, "application/x-www-form-urlencoded");
        return this;
    }
    public IHttpRequestBuilder AddHeader(string key, string value)
    {
        _headers.Add(key, value);
        return this;
    }

    public HttpRequestMessage Build()
    {
        var message = new HttpRequestMessage(_method, _uri);
        if (_content is not null)
        {
            message.Content = _content;
        }
        foreach (var header in _headers)
        {
            message.Headers.TryAddWithoutValidation(header.Key, header.Value);
        }
        return message;
    }
}