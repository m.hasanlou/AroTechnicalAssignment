﻿using System.Net;

namespace Tripadvisor.Infrastructure.HttpUtility;

public class HttpResponse
{
    public bool IsSuccess { get; set; }
    public string RequestContent { get; set; }
    public string Unique { get; set; }
    public HttpStatusCode StatusCode { get; set; }
    public string Message { get; set; }
}

public class HttpResponse<T> : HttpResponse
{
    public T Result { get; set; }
}