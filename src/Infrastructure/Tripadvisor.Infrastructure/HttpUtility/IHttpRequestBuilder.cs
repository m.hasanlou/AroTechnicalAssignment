﻿namespace Tripadvisor.Infrastructure.HttpUtility;

public interface IHttpRequestBuilder
{
    IHttpRequestBuilder WithUri(string uri);
    IHttpRequestBuilder WithAccessToken(string token);
    IHttpRequestBuilder WithGetVerb();
    IHttpRequestWithContentBuilder WithPostVerb();
    IHttpRequestWithContentBuilder WithPutVerb();
    IHttpRequestWithContentBuilder WithPatchVerb();
    IHttpRequestBuilder WithDeleteVerb();
    HttpRequestMessage Build();
}

public interface IHttpRequestWithContentBuilder : IHttpRequestBuilder
{
    IHttpRequestWithContentBuilder AddJsonContent(object data);
    IHttpRequestWithContentBuilder AddFormContent(object data);
    IHttpRequestWithContentBuilder AddXmlContent(object data);
}