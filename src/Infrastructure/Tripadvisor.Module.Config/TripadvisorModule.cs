﻿using Autofac;
using Microsoft.Extensions.Configuration;
using System.Reflection;
using Tripadvisor.Application.Contract.Framework;
using Tripadvisor.Application.Framework;
using Tripadvisor.Application.Locations.Commands;
using Tripadvisor.Domain.Framework;
using Tripadvisor.Domain.Locations;
using Tripadvisor.Infrastructure;
using Tripadvisor.Infrastructure.ACL;
using Tripadvisor.Infrastructure.HttpUtility;
using Tripadvisor.Persistence.EF;
using Tripadvisor.Persistence.EF.Repositories;

namespace Tripadvisor.Module.Config
{
    public class TripadvisorModule : Autofac.Module
    {
        private readonly IConfiguration _configuration;

        public TripadvisorModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(CreateDbContext).InstancePerLifetimeScope();

            builder.RegisterType<UnitOfWork>()
                .As<IUnitOfWork>()
                .InstancePerLifetimeScope();

            builder.RegisterType<LocationBenchmarkRepository>()
                .As<ILocationBenchmarkRepository>()
                .InstancePerLifetimeScope();

            builder.RegisterDecorator(typeof(LocationBenchmarkRepositoryProxy), typeof(ILocationBenchmarkRepository));

            builder.RegisterDecorator(typeof(LocationPopularityRetrieverProxy), typeof(ILocationPopularityRetriever));

            builder.RegisterType<CommandBus>()
                .As<ICommandBus>()
                .InstancePerLifetimeScope();

            builder.RegisterType<QueryBus>()
                .As<IQueryBus>()
                .InstancePerLifetimeScope();

            builder.RegisterType<MemoryCacheManager>()
                .As<ICacheManager>()
                .SingleInstance();

            builder.RegisterType<ApiCaller>()
                .SingleInstance();

            builder.RegisterAssemblyTypes(GetAssemblyOfCommands())
                .As(type => type.GetInterfaces()
                    .Where(interfaceType => interfaceType.IsClosedTypeOf(typeof(ICommandHandler<>))))
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(GetAssemblyOfCommands())
                .As(type => type.GetInterfaces()
                    .Where(interfaceType => interfaceType.IsClosedTypeOf(typeof(ICommandHandler<,>))))
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(GetAssemblyOfCommands())
                .As(type => type.GetInterfaces()
                    .Where(interfaceType => interfaceType.IsClosedTypeOf(typeof(IQueryHandler<,>))))
                .InstancePerLifetimeScope();
        }

        private TripadvisorDbContext CreateDbContext(IComponentContext arg)
        {
            var connection = _configuration.GetConnectionString("Default");
            return TripadvisorDbContextFactory.Create(connection);
        }

        private Assembly GetAssemblyOfCommands()
        {
            return typeof(EstablishLocationBenchmarkCommandHandler).Assembly;
        }
    }
}