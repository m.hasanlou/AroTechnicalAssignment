﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tripadvisor.Domain.Locations;

namespace Tripadvisor.Persistence.EF.Configurations
{
    public class LocationBenchmarkConfiguration : IEntityTypeConfiguration<LocationBenchMark>
    {
        public void Configure(EntityTypeBuilder<LocationBenchMark> builder)
        {
            builder.ToTable("LocationBenchMarks")
                .HasKey(e => e.Id);

            builder.Property(e => e.Id)
                .ValueGeneratedOnAdd();

            builder.Property(e => e.GeoLocationId)
                .IsRequired();

            builder.Property(e => e.Rank)
                .HasDefaultValue(0)
                .IsRequired();

            builder.HasIndex(e => e.GeoLocationId)
                .IsUnique();
        }
    }
}
