﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Tripadvisor.Domain.Locations;

namespace Tripadvisor.Persistence.EF
{
    public class TripadvisorDbContext : DbContext
    {
        public TripadvisorDbContext(DbContextOptions<TripadvisorDbContext> options)
            : base(options)
        {
        }

        public DbSet<LocationBenchMark> LocationBenchMarks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(TripadvisorDbContext).Assembly);

            base.OnModelCreating(modelBuilder);
        }
    }
}
