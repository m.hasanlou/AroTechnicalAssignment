﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Tripadvisor.Persistence.EF;

public static class TripadvisorDbContextFactory
{
    public static TripadvisorDbContext Create(string connectionString)
    {
        var builder = new DbContextOptionsBuilder<TripadvisorDbContext>();
        builder.UseSqlServer(connectionString);

        return new TripadvisorDbContext(builder.Options);
    }

    public class TripadvisorDesignTimeDbContextFactory :
        IDesignTimeDbContextFactory<TripadvisorDbContext>
    {
        public TripadvisorDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<TripadvisorDbContext>();
            builder.UseSqlServer("data source=DESKTOP-H08AQQF\\SQL2019;integrated security=true;initial catalog=Aro");

            return new TripadvisorDbContext(builder.Options);
        }
    }
}