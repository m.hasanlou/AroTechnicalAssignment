﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Tripadvisor.Domain.Framework;

namespace Tripadvisor.Persistence.EF.Repositories;

public abstract class BaseRepository<TEntity, TKey>
    where TEntity : Entity<TKey>
{
    protected TripadvisorDbContext dbContext;
    protected DbSet<TEntity> dbSet => dbContext.Set<TEntity>();

    protected BaseRepository(TripadvisorDbContext context)
    {
        dbContext = context;
    }

    public Task<List<TEntity>> Get(
        Expression<Func<TEntity, bool>>? filter = null,
        Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
        string includeProperties = "",
        CancellationToken cancellation = default)
    {
        IQueryable<TEntity> query = dbSet;

        if (filter is not null)
        {
            query = query.Where(filter);
        }

        foreach (var includeProperty in includeProperties.Split
                     (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
        {
            query = query.Include(includeProperty);
        }

        return orderBy != null ?
            orderBy(query).ToListAsync(cancellation) :
            query.ToListAsync(cancellation);
    }

    public Task<TEntity> Get(TKey id, CancellationToken cancellation = default)
    {
        return dbSet.FirstOrDefaultAsync(c =>
            c.Id.Equals(id), cancellation);
    }

    public async Task<TEntity> Add(TEntity entity, CancellationToken cancellation = default)
    {
        var ent = await dbSet.AddAsync(entity, cancellation);
        return ent.Entity;
    }

    public Task<bool> IsRemoved(TKey id, CancellationToken cancellation = default)
    {
        return dbSet.AnyAsync(c => c.Removed, cancellation);
    }

    public Task<bool> Exists(TKey id, CancellationToken cancellation = default)
    {
        return dbSet.AnyAsync(c => c.Id.Equals(id), cancellation);
    }
}