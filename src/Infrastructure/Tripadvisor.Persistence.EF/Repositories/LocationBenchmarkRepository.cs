﻿using Microsoft.EntityFrameworkCore;
using Tripadvisor.Domain.Locations;

namespace Tripadvisor.Persistence.EF.Repositories
{
    public class LocationBenchmarkRepository :
        BaseRepository<LocationBenchMark, int>, ILocationBenchmarkRepository
    {
        public LocationBenchmarkRepository(TripadvisorDbContext context)
            : base(context)
        {
        }

        public Task<LocationBenchMark?> GetByLocation(
            int locationId, CancellationToken cancellation = default)
        {
            return dbSet.FirstOrDefaultAsync(e =>
                e.GeoLocationId == locationId, cancellation);
        }
    }
}
