﻿using Tripadvisor.Domain.Framework;
using Tripadvisor.Domain.Locations;

namespace Tripadvisor.Persistence.EF.Repositories;

public class LocationBenchmarkRepositoryProxy : ILocationBenchmarkRepository
{
    private readonly ILocationBenchmarkRepository _repository;
    private readonly ICacheManager _cacheManager;

    public LocationBenchmarkRepositoryProxy(
        ILocationBenchmarkRepository repository,
        ICacheManager cacheManager)
    {
        _repository = repository;
        _cacheManager = cacheManager;
    }

    public async Task<LocationBenchMark?> GetByLocation(
        int locationId, CancellationToken cancellation = default)
    {
        var key = $"{nameof(LocationBenchMark)}_{locationId}";
        var locationBenchmark =  _cacheManager.Get<LocationBenchMark>(key);

        if (locationBenchmark is not null)
        {
            return locationBenchmark;
        }

        locationBenchmark = await _repository.GetByLocation(locationId, cancellation);
        if (locationBenchmark is not null)
        {
            _cacheManager.Set(key, locationBenchmark);
        }

        return locationBenchmark;
    }

    public Task<LocationBenchMark> Add(
        LocationBenchMark model, CancellationToken cancellation = default)
    {
        return _repository.Add(model, cancellation);
    }
}