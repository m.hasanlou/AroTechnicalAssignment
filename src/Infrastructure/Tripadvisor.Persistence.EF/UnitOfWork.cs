﻿using Tripadvisor.Domain.Framework;
using Tripadvisor.Domain.Locations;
using Tripadvisor.Persistence.EF.Repositories;

namespace Tripadvisor.Persistence.EF;

public class UnitOfWork: IUnitOfWork
{
    private readonly TripadvisorDbContext _dbContext;

    private ILocationBenchmarkRepository _locationBenchmarkRepository;

    public UnitOfWork(TripadvisorDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public ILocationBenchmarkRepository LocationBenchmarkRepository
    {
        get
        {
            _locationBenchmarkRepository ??= new LocationBenchmarkRepository(_dbContext);
            return _locationBenchmarkRepository;
        }
    }

    public Task<int> SaveChanges(CancellationToken cancellation = default)
    {
        return _dbContext.SaveChangesAsync(cancellation);
    }
}