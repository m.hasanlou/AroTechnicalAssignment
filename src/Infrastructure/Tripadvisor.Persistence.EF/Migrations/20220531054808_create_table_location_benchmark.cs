﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Tripadvisor.Persistence.EF.Migrations
{
    public partial class create_table_location_benchmark : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LocationBenchMarks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GeoLocationId = table.Column<int>(type: "int", nullable: false),
                    Rank = table.Column<byte>(type: "tinyint", nullable: false, defaultValue: (byte)0),
                    Removed = table.Column<bool>(type: "bit", nullable: false),
                    CreationDate = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LocationBenchMarks", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LocationBenchMarks_GeoLocationId",
                table: "LocationBenchMarks",
                column: "GeoLocationId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LocationBenchMarks");
        }
    }
}
