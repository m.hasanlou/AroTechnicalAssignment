using Autofac;
using Autofac.Extensions.DependencyInjection;
using Tripadvisor.Infrastructure.ACL;
using Tripadvisor.Domain.Locations;
using Tripadvisor.Module.Config;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory())
    .ConfigureContainer<ContainerBuilder>(containerBuilder =>
    {
        containerBuilder.RegisterModule(new TripadvisorModule(builder.Configuration));
    });

builder.Services.AddAutofac();
builder.Services.AddHttpClient<ILocationPopularityRetriever, LocationPopularityRetriever>(client =>
    {
        client.BaseAddress = new Uri(builder.Configuration["TripadvisorUrl"]);
    });

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
